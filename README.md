﻿# PDFPageNumbers

## Component Information
* Author: Jonathan Hult
* Website: https://jonathanhult.com
* Last Updated: build_2_08082015

## Overview
This component counts the number of pages in a PDF and stores this value in a metadata field defined in a preference prompt.
	
This component implements two filters: extraAfterServicesLoadInit and validateStandard. It uses extraAfterServicesLoadInit to install the metadata field which will store the number of pages in the PDF. It uses validateStandard and the iText library to count the number of pages in the PDF and store it in the metadata field.

* Filters:
	- validateStandard: Executed during every metadata update and checkin.
	- extraAfterServicesLoadInit: Executed after all the services are loaded

* Tracing Sections:
	- pdfpagenumbers
	
* Preference Prompts:
	- PDFPageNumbers_ComponentEnabled - Boolean determining if the component functionality is enabled.
	- PDFPageNumbers_MetadataFieldName: Metadata field name for PDF number of pages.
	- PDFPageNumbers_MetadataFieldCaption: Metadata field caption.
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.
* 10.1.3.5.1 (111229) (Build: 7.2.4.105)

## Changelog
* build_2_08082015
	- Fixed issue with uppercase PDF extension
	- Upgraded iText from 5.4.1 to 5.5.6
	- Upgraded Bouncy Castle from bcprov-jdk15on-148 to bcprov-jdk15on-152
* build_1_20130109
	- Initial component release