package com.jonathanhult.webcenter.content.pdfpagenumbers;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.EXTRA_AFTER_SERVICES_LOAD_INIT;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.FILTER_PARAMETER;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.addPDFNumberOfPagesMetadataField;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.traceVerbose;
import intradoc.common.ExecutionContext;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.IdcExtendedLoader;
import intradoc.shared.FilterImplementor;
/*
 * @author Jonathan Hult
 */
public class PDFPageNumbersExtraAfterServicesLoadInitFilter implements FilterImplementor {

	/**
	 * This method implements a extraAfterServicesLoadInit filter to add metadata fields needed for this component.
	 */
	public int doFilter(Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		traceVerbose("Start doFilter for extraAfterServicesLoadInit for ChecksumContent");

		try {
			// Exit if parameter is not found
			final Object paramObj = ctx.getCachedObject(FILTER_PARAMETER);
			if (paramObj == null || (paramObj instanceof String) == false) {
				return CONTINUE;
			}
			final String param = (String)paramObj;

			if (ctx instanceof IdcExtendedLoader) {
				final IdcExtendedLoader loader = (IdcExtendedLoader) ctx;
				if (ws == null) {
					ws = loader.getLoaderWorkspace();
				}
			}

			if (param.equals(EXTRA_AFTER_SERVICES_LOAD_INIT)) {
				// Add metadata field
				addPDFNumberOfPagesMetadataField(ws, binder, ctx);
			}
			return CONTINUE;
		} finally {
			traceVerbose("End doFilter for extraAfterServicesLoadInit for ChecksumContent");
		}
	}
}
