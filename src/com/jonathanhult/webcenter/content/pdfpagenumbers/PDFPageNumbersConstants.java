package com.jonathanhult.webcenter.content.pdfpagenumbers;

public class PDFPageNumbersConstants {
	protected static final String COMPONENT_NAME = "PDFPageNumbers";
	protected static final String TRACE_SECTION = "pdfpagenumbers";
	protected static final String METADATA_FIELD_NAME = "PDFPageNumbers_MetadataFieldName";
	protected static final String METADATA_FIELD_CAPTION = "PDFPageNumbers_MetadataFieldCaption";
	protected static final String COMPONENT_ENABLED = "PDFPageNumbers_ComponentEnabled";
	protected static final String PRIMARY_FILE_PATH = "primaryFile:path";
	protected static final String PDF = ".pdf";
	protected static final String UPDATE_META_TABLE = "UPDATE_META_TABLE";
	protected static final String FILTER_PARAMETER = "filterParameter";
	protected static final String EXTRA_AFTER_SERVICES_LOAD_INIT = "extraAfterServicesLoadInit";
	protected static final String CURRENT_BUILD = "build_1_20130109";
	protected static final String INT = "Int";
	protected static final String D_NAME = "dName";
	protected static final String D_CAPTION = "dCaption";
	protected static final String X = "x";
}
