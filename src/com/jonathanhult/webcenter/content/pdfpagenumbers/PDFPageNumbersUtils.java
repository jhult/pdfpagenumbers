package com.jonathanhult.webcenter.content.pdfpagenumbers;

import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.COMPONENT_NAME;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.CURRENT_BUILD;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.D_CAPTION;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.D_NAME;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.INT;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.METADATA_FIELD_CAPTION;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.METADATA_FIELD_NAME;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.TRACE_SECTION;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.X;
import static intradoc.shared.MetaFieldUtils.hasDocMetaDef;
import static intradoc.shared.MetaFieldUtils.updateMetaDataFromProps;
import static intradoc.shared.SharedObjects.getEnvironmentValue;
import intradoc.common.ExecutionContext;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;

import java.util.Properties;

/*
 * @author Jonathan Hult
 */
public class PDFPageNumbersUtils {
	/**
	 * This method adds a metadata field to the system needed for storing the
	 * number of pages. This is called from the extraAfterServicesLoadInit
	 * filter.
	 * 
	 * @param ws
	 * @param binder
	 */
	public static void addPDFNumberOfPagesMetadataField(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) {
		traceVerbose("Start addPDFNumberOfPagesMetadataField");

		// Parameters for metadata field
		final String[] parameters = new String[] { "dsdComponentName", "dComponentName", "dsdVersion", "dType", "dIsRequired", "dIsEnabled", "dIsSearchable", "dIsOptionList", "dOptionListKey", "dOptionListType", "dDefaultValue", "dOrder", "dIsPlaceholderField", "dsdCheckFlag",
				"dsdDisableOnUninstall" };

		// Common values for field
		final String[] values = new String[] { COMPONENT_NAME, COMPONENT_NAME, CURRENT_BUILD, INT, "0", "1", "1", "0", "", "", "", "2500", "0", "", "" };

		// PDF page numbers field parameters and values
		final Properties field = new Properties();
		for (int i = 0; i < parameters.length; i++) {
			field.put(parameters[i], values[i]);
		}

		// Setup primary file specific parameters
		field.put(D_NAME, getMetadataFieldName());
		field.put(D_CAPTION, getEnvironmentValue(METADATA_FIELD_CAPTION));

		traceVerbose("Field: " + field.toString());

		try {
			// Name of metadata field
			final String fieldName = field.getProperty(D_NAME);
			trace(D_NAME + ": " + fieldName);

			if (fieldName != null && fieldName.length() > 0 && !hasDocMetaDef(fieldName)) {
				try {
					updateMetaDataFromProps(ws, null, field, fieldName, true);
					trace("Successfully added metadata field " + fieldName);
				} catch (final ServiceException e) {
					warn("Metadata field " + fieldName + " was not installed.", e);
				} catch (final DataException e) {
					warn("Metadata field " + fieldName + " was not installed.", e);
				}
			} else {
				trace("Metadata field already exists; will not modify");
			}
		} finally {
			traceVerbose("End addPDFNumberOfPagesMetadataField");
		}
	}

	/**
	 * This method gets the number of pages field name.
	 * 
	 * @return String containing preference prompt value.
	 */
	public static String getMetadataFieldName() {
		final StringBuilder toReturn = new StringBuilder(X);
		toReturn.append(getEnvironmentValue(METADATA_FIELD_NAME));

		if (toReturn.length() > 30) {
			toReturn.setLength(30);
		}
		return toReturn.toString();
	}

	public static void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	public static void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}

	public static void warn(final String message, final Exception exception) {
		Report.warning(TRACE_SECTION, message, exception);
	}
}
