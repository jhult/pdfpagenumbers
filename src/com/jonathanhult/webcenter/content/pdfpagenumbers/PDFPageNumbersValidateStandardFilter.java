package com.jonathanhult.webcenter.content.pdfpagenumbers;

import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.COMPONENT_ENABLED;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.PDF;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersConstants.PRIMARY_FILE_PATH;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.getMetadataFieldName;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.trace;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.traceVerbose;
import static com.jonathanhult.webcenter.content.pdfpagenumbers.PDFPageNumbersUtils.warn;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.ExecutionContext;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.shared.FilterImplementor;

import java.io.IOException;

import com.itextpdf.text.pdf.PdfReader;

/*
 * @author Jonathan Hult
 */
public class PDFPageNumbersValidateStandardFilter implements FilterImplementor {

	@Override
	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		try {
			traceVerbose("Start doFilter for validateStandard for PDFPageNumbers");

			// Exit if component is not enabled
			if (!getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				return CONTINUE;
			}

			// Get primaryFile:path
			final String primaryFilePath = binder.getLocal(PRIMARY_FILE_PATH);

			// Exit if primaryFile:path is missing
			if (primaryFilePath == null || primaryFilePath.trim().length() < 1) {
				trace("Could not find primary file path; will not calculate number of pages");
				return CONTINUE;
			}

			// Exit if primary file is not a PDF
			if (!primaryFilePath.toLowerCase().endsWith(PDF)) {
				trace("Primary file is not a PDF; will not calculate number of pages");
				return CONTINUE;
			}

			traceVerbose("primaryFilePath: " + primaryFilePath);

			// Get PDF reader object
			final PdfReader reader = new PdfReader(primaryFilePath);

			// Get number of pages in PDF
			final int numPages = reader.getNumberOfPages();
			trace("numPages: " + numPages);

			// Get metadata field to store number of pages in
			// This is retrieved from a preference prompt
			final String metadataFieldName = getMetadataFieldName();

			if (metadataFieldName == null || metadataFieldName.trim().length() < 1) {
				trace("Metadata field could not be found; will not calculate number of pages");
				return CONTINUE;
			}

			// Store number of pages in metadata field
			binder.putLocal(metadataFieldName, String.valueOf(numPages));
		} catch (final IOException e) {
			warn("Could not read PDF. Page numbers will not be stored in metadata.", e);
		} finally {
			traceVerbose("End doFilter for validateStandard for PDFPageNumbers");
		}
		return CONTINUE;
	}

}
